#include <iostream>

bool cuadrado_despiece(int numero){
	
	if (numero < 99){
		return false;
	}

	int ultimosDigitos = numero % 100;
	int primerosDigitos = numero / 100;
	int suma = ultimosDigitos + primerosDigitos;
	if (suma*suma == numero ){
		return true;
	}else{
		return false;
	}
}


int main( ){
	int numero = 0;
	std::cout<<"Cuadrado despiece"<<std::endl;
	std::cout<<"Digite numero: ";
	std::cin>>numero;

	if ( cuadrado_despiece(numero) ){
		std::cout << "cumple con las condiciones";
	}else{
		std::cout << "no cumple con las condiciones";
	}

}
