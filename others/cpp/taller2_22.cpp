#include <iostream>
using namespace std;
 
void imprimirLista(int tamanio, int lista[], int comb){
    cout<<"Combinacion # "<<comb<<"  \t";
    for (int i = 0; i < tamanio; i++)
        cout<<lista[i]<<" ";
    cout<<endl;
}
 
 
void posibilidades(int numero, int iteracion){
    static int lista[50];
    static int comb = 0;
    if (numero == 0){
	comb+=1;
        imprimirLista(iteracion, lista, comb);
    }else if(numero > 0){
        for (int puntos = 3; puntos <= 7; puntos+=2){
            lista[iteracion]= puntos;
            posibilidades(numero-puntos, iteracion+1);
        }
    }
}
 
int main(){

    int numero = 0;
    cout<<"> Ingrese puntaje ";
    cin>>numero;

    posibilidades(numero, 0);

}
