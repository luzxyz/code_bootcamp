#include <iostream>


bool es_bisiesto(int anho){

  if (anho % 4 == 0 && anho % 100 == 0 && anho % 400 == 0){
     return true;    
  }else{
     return false;
   }
 
}

void convertir_long_a_fecha(long fecha, int *anho, int *mes, int *dia){
 
 *dia = fecha % 100;
 fecha = fecha - *dia;
 *mes = fecha % 10000;
 fecha = fecha - *mes;
 *mes = *mes / 100;
 *anho = fecha / 10000;
}

int dias_de_mes(int mes, int anho){
  if (anho == 2){
     if (! es_bisiesto( anho )){
        return 28;
      }else{
        return 29;
      }
  }else if (anho == 1 || anho == 3 || anho == 5
    || anho == 7 || anho == 8 || anho ==  10 || anho == 12){
    return 31;
  }else{
    return 30;
  }
}

void cantidad_dias(long fecha1, long fecha2){

    int dia1 = 0;
    int dia2 = 0;
    int mes1 = 0;
    int mes2 = 0;
    int anho1 = 0;
    int anho2 = 0;
    
    int cuentainicial = 0;
    int cuentafinal = 0;
    int total_dias = 0;
    
    convertir_long_a_fecha( fecha1, &anho1, &mes1, &dia1);
    convertir_long_a_fecha( fecha2, &anho2, &mes2, &dia2);
    
    if (anho1 != anho2){

       if (es_bisiesto(anho2)){
         cuentainicial = 366;
       }else{
         cuentainicial = 365;
       }

        for (int i = 0; i < mes2-1; i ++ ){
          cuentainicial = cuentainicial - dias_de_mes(i, anho2);
        }
	
       cuentainicial = cuentainicial - dia2;
    } 
      
     
    for (int i = 0; i < mes1-1; i ++ ){
      cuentafinal = cuentafinal + dias_de_mes(i, anho1);
    }
    
    
    cuentafinal = cuentafinal + dia1;

    for (int i = anho2+1; i < anho1; i++){
       if (es_bisiesto(i)){
         total_dias = total_dias + 366;
       }else{
         total_dias = total_dias + 365;
       }
    }
    
    total_dias = total_dias + cuentainicial + cuentafinal;

   std::cout<<"La cantidad total de dias es: "<<total_dias<<" !";
}

int main(){

    long fecha1;
    long fecha2;

    std::cout<<"Ingrese fecha 1 (la final): ";

    std::cin >> fecha1;
    
    std::cout<<"Ingrese fecha 2 (la inicial): ";
    std::cin >> fecha2;
    
    cantidad_dias ( fecha1, fecha2);

}
